<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("controller.main", id="controller.main")
 * @Route("/", service="controller.main")
 */
class MainController
{
    function __construct () {
    }
    
    /**
     * @Route("/", name="root")
     * @Template()
     * @return array data
     */
    public function rootAction()
    {
        return array();
    }
}
