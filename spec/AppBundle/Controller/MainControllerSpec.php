<?php

namespace spec\AppBundle\Controller;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MainControllerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('AppBundle\Controller\MainController');
    }
    
    function it_should_generate_a_default_response ()
    {
        $this->rootAction()->shouldBe(array());
    }
}
